<?php

use Phalcon\DI\FactoryDefault,
    Phalcon\Mvc\Micro,
    Phalcon\Http\Response,
    Phalcon\Http\Request;

$di = new FactoryDefault();

//Using an anonymous function, the instance will be lazy loaded
$di["response"] = function () {
	return new Response();
};

$di["request"] = function() {
	return new Request();
};

$app = new Micro();
$app->setDI($di);

$app->get('/aboutme', function () use ($app) {
	echo '{"name":"App Server", "version":"0.0.1"';
});

$app->get('/api/v0/receiver/all', function () use ($app) {
	$link = mysql_connect('localhost', 'root', 'remi')
        or die('Could not connect: ' . mysql_error());

	mysql_select_db('push_db') or die('Could not select database');

	$query = 'SELECT reg_id FROM user_push';
	$result = mysql_query($query) or die('Query failed: ' . mysql_error());
    
	if (!$result){
		$response = array("result"=>"failed",
		"reason"=>"something with DB");
	}else{ 
		$response = array("list"=>"");
		$counter = 0;
		while ($line = mysql_fetch_array($result, MYSQL_ASSOC)) {
			foreach ($line as $col_value) {
				$response["list"][] = $col_value;
			}
		}
	}
    
	echo json_encode($response );	
	mysql_free_result($result);
	mysql_close($link);
});

$app->post('/api/v0/receiver/reg', function() use ($app) {
	$post_payload = $app->request->getRawBody();
	$json_obj = json_decode($post_payload);
	$new_reg_id = $json_obj->{'reg_id'};
	$new_model = "new_model";
    
	if (!$new_reg_id)
		die('{"result":"fail-reg_id"}');

	$link = mysql_connect('localhost', 'root', 'remi')
        or die('Could not connect: ' . mysql_error());

	mysql_select_db('push_db') or die('Could not select database');

	//$query = "INSERT INTO `user_push` (`reg_id`, `model`) VALUES (\"$new_reg_id\", \"$new_model\")";
	//$result = mysql_query($query) or die('Query failed: ' . mysql_error());

	if (!$result){
		$response = array("result"=>"failed",
		"reason"=>"something with DB");
	}else{ 
		$response = array("result"=>"success");
	}
	echo json_encode( $response );
});


$app->post('/api/v0/msg/send', function() use ($app) {
	$post_payload = $app->request->getRawBody();
	$json_obj = json_decode($post_payload,true);
	
	$new_receiver = $json_obj["receiver"];
	$new_sender = $json_obj["msg"][0]["sender"];  
	$new_data = $json_obj["msg"][0]["data"];

	if (!$new_receiver)
		die('{"result":"fail-receiver"}');
	if (!$new_sender)
		die('{"result":"fail-sender"}');
	if (!$new_data)
		die('{"result":"fail-data"}');

	$link = mysql_connect('localhost', 'root', 'remi')
        or die('Could not connect: ' . mysql_error());

	mysql_select_db('push_db') or die('Could not select database');

	//$query = "INSERT INTO `sent_messages` (`sender_push`, `receiver_push`, `message_push`, `status`) VALUES (\"$new_sender\", \"$new_receiver\", \"$new_data\", '0')";
	//$result = mysql_query($query) or die('Query failed: ' . mysql_error());

	if (!$result){
		$response = array("result"=>"failed",
		"reason"=>"something with DB");
	}else{ 
		$response = array("result"=>"success");
	}
	echo json_encode($response);
});


$app->notFound(function () use ($app) {
	$app->response->setStatusCode(404, "Not Found")->sendHeaders();
	echo '{"result":"fail"}';
});

$app->handle();

?>
